# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/robotino/robotino_ws/src/urg_node/src/urg_node.cpp" "/home/robotino/robotino_ws/build/urg_node/CMakeFiles/urg_node.dir/src/urg_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"urg_node\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/robotino/robotino_ws/devel/.private/urg_node/include"
  "/home/robotino/robotino_ws/src/urg_node/include"
  "/home/robotino/robotino_ws/devel/.private/tf2_msgs/include"
  "/home/robotino/robotino_ws/src/laser_proc/include"
  "/home/robotino/robotino_ws/src/geometry2/tf2_msgs/include"
  "/home/robotino/robotino_ws/src/urg_c/current/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/robotino/robotino_ws/build/urg_node/CMakeFiles/urg_node_driver.dir/DependInfo.cmake"
  "/home/robotino/robotino_ws/build/urg_node/CMakeFiles/urg_c_wrapper.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
