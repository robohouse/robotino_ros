#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/robotino/robotino_ws/devel/.private/voxel_grid:$CMAKE_PREFIX_PATH"
export PWD="/home/robotino/robotino_ws/build/voxel_grid"
export ROSLISP_PACKAGE_DIRECTORIES="/home/robotino/robotino_ws/devel/.private/voxel_grid/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/robotino/robotino_ws/src/navigation/voxel_grid:$ROS_PACKAGE_PATH"