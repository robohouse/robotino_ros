# CMake generated Testfile for 
# Source directory: /home/robotino/robotino_ws/src/image_common/camera_calibration_parsers
# Build directory: /home/robotino/robotino_ws/build/camera_calibration_parsers
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(test)
