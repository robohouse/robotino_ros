# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/msg/AnalogReadings.msg;/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/msg/EncoderReadings.msg;/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/msg/MotorReadings.msg;/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/msg/DigitalReadings.msg;/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/msg/PowerReadings.msg"
services_str = "/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/srv/ResetOdometry.srv;/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/srv/SetEncoderPosition.srv"
pkg_name = "robotino_node"
dependencies_str = "std_msgs;geometry_msgs"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "robotino_node;/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/msg;std_msgs;/opt/ros/kinetic/share/std_msgs/cmake/../msg;geometry_msgs;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/kinetic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
