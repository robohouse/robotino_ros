# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_connection.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_connection.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_debug.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_debug.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_ring_buffer.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_ring_buffer.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_sensor.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_sensor.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_serial.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_serial.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_serial_utils.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_serial_utils.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_tcpclient.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_tcpclient.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_time.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_time.c.o"
  "/home/robotino/robotino_ws/src/urg_c/current/src/urg_utils.c" "/home/robotino/robotino_ws/build/urg_c/CMakeFiles/liburg_c.dir/current/src/urg_utils.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/robotino/robotino_ws/src/urg_c/current/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
