#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/robotino/robotino_ws/devel/.private/camera_calibration_parsers:$CMAKE_PREFIX_PATH"
export PWD="/home/robotino/robotino_ws/build/camera_calibration_parsers"
export ROSLISP_PACKAGE_DIRECTORIES="/home/robotino/robotino_ws/devel/.private/camera_calibration_parsers/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/robotino/robotino_ws/src/image_common/camera_calibration_parsers:$ROS_PACKAGE_PATH"