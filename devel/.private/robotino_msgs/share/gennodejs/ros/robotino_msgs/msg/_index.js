
"use strict";

let SetBHAPressures = require('./SetBHAPressures.js');
let EncoderReadings = require('./EncoderReadings.js');
let MotorReadings = require('./MotorReadings.js');
let DigitalReadings = require('./DigitalReadings.js');
let PowerReadings = require('./PowerReadings.js');
let SetGrapplerAxis = require('./SetGrapplerAxis.js');
let GripperState = require('./GripperState.js');
let SetGrapplerAxes = require('./SetGrapplerAxes.js');
let GrapplerReadings = require('./GrapplerReadings.js');
let AnalogReadings = require('./AnalogReadings.js');
let NorthStarReadings = require('./NorthStarReadings.js');
let BHAReadings = require('./BHAReadings.js');

module.exports = {
  SetBHAPressures: SetBHAPressures,
  EncoderReadings: EncoderReadings,
  MotorReadings: MotorReadings,
  DigitalReadings: DigitalReadings,
  PowerReadings: PowerReadings,
  SetGrapplerAxis: SetGrapplerAxis,
  GripperState: GripperState,
  SetGrapplerAxes: SetGrapplerAxes,
  GrapplerReadings: GrapplerReadings,
  AnalogReadings: AnalogReadings,
  NorthStarReadings: NorthStarReadings,
  BHAReadings: BHAReadings,
};
