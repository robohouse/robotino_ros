# CMake generated Testfile for 
# Source directory: /home/robotino/robotino_ws/src/image_common/camera_calibration_parsers/test
# Build directory: /home/robotino/robotino_ws/build/camera_calibration_parsers/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_camera_calibration_parsers_nosetests_parser.py "/home/robotino/robotino_ws/build/camera_calibration_parsers/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/robotino/robotino_ws/build/camera_calibration_parsers/test_results/camera_calibration_parsers/nosetests-parser.py.xml" "--return-code" "/usr/bin/cmake -E make_directory /home/robotino/robotino_ws/build/camera_calibration_parsers/test_results/camera_calibration_parsers" "/usr/bin/nosetests-2.7 -P --process-timeout=60 /home/robotino/robotino_ws/src/image_common/camera_calibration_parsers/test/parser.py --with-xunit --xunit-file=/home/robotino/robotino_ws/build/camera_calibration_parsers/test_results/camera_calibration_parsers/nosetests-parser.py.xml")
