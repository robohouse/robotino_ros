#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/robotino/robotino_ws/devel/.private/polled_camera:$CMAKE_PREFIX_PATH"
export PWD="/home/robotino/robotino_ws/build/polled_camera"
export ROSLISP_PACKAGE_DIRECTORIES="/home/robotino/robotino_ws/devel/.private/polled_camera/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/robotino/robotino_ws/src/image_common/polled_camera:$ROS_PACKAGE_PATH"