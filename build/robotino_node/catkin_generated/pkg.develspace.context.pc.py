# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/robotino/robotino_ws/devel/.private/robotino_node/include".split(';') if "/home/robotino/robotino_ws/devel/.private/robotino_node/include" != "" else []
PROJECT_CATKIN_DEPENDS = "image_transport;nav_msgs;tf;message_runtime;geometry_msgs;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "robotino_node"
PROJECT_SPACE_DIR = "/home/robotino/robotino_ws/devel/.private/robotino_node"
PROJECT_VERSION = "0.1.0"
