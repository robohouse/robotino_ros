# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/src/CameraROS.cpp" "/home/robotino/robotino_ws/build/robotino_node/CMakeFiles/robotino_camera_node.dir/src/CameraROS.cpp.o"
  "/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/src/ComROS.cpp" "/home/robotino/robotino_ws/build/robotino_node/CMakeFiles/robotino_camera_node.dir/src/ComROS.cpp.o"
  "/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/src/RobotinoCameraNode.cpp" "/home/robotino/robotino_ws/build/robotino_node/CMakeFiles/robotino_camera_node.dir/src/RobotinoCameraNode.cpp.o"
  "/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/src/robotino_camera_node.cpp" "/home/robotino/robotino_ws/build/robotino_node/CMakeFiles/robotino_camera_node.dir/src/robotino_camera_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"robotino_node\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/robotino/robotino_ws/devel/.private/robotino_node/include"
  "/home/robotino/robotino_ws/src/catkin-pkg/robotino_node/include"
  "/home/robotino/robotino_ws/devel/.private/tf2_msgs/include"
  "/home/robotino/robotino_ws/src/image_common/image_transport/include"
  "/home/robotino/robotino_ws/src/geometry2/tf2_msgs/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
